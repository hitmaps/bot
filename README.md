# HITMAPS Bot
This repo contains the code used for the HITMAPS Discord bot.

## Configuration
Properties can be configured either via a `config.json` in the root folder, or via environment variables.
When using the latter, the environment variable's name should match the `config.json.example` file's JSON key.