const { SlashCommandBuilder } = require('@discordjs/builders');
const { default: axios } = require('axios');
const { MessageEmbed } = require('discord.js');

module.exports = {
    commandType: 'GLOBAL',
    data: async () => {
        return new SlashCommandBuilder()
            .setName('botstatus')
            .setDescription('Retrieves the current status of the HITMAPS bot regarding the contract / leaderboard commands')
    },
    async execute(interaction) {
        await interaction.deferReply();
        const url = `https://ioiapi.hitmaps.com/api/platform-status`;
        let response = null;

        try {
            response = await axios.get(url);
        } catch (e) {
            return await interaction.editReply(':x: An error occurred when fetching statuses');
        }

        let worstHealth = 1;
        const statuses = [];

        for (const entry of response.data.data) {
            statuses.push({
                name: entry.platform,
                status: getServerHealth(entry.status)
            });
            worstHealth = Math.max(worstHealth, getStatusId(entry.status))
        }

        const embed = new MessageEmbed()
            .setColor(getEmbedColor(worstHealth))
            .setTitle('HITMAPS Contracts / Leaderboard Bot Status')
            .setFooter({ text: 'Bot availability is subject to community members being willing/able to help keep the bot authenticated.\nWant to help keep the bot authenticated?  Visit https://bot.hitmaps.com' })
            
        for (const status of statuses) {
            embed.addField(status.name, status.status, true);
        }

        return await interaction.editReply({ embeds: [embed] });
    }
};

function getServerHealth(status) {
    if (status === 'online') {
        return ':white_check_mark: Online';
    }
    if (status === 'stale') {
        return ':warning: Stale';
    }
    if (status === 'offline') {
        return ':x: Offline';
    }

    return ':man_shrugging: Unknown';
}

function getStatusId(status) {
    if (status === 'online') {
        return 1;
    }
    if (status === 'stale') {
        return 3;
    }
    if (status === 'offline') {
        return 4;
    }

    // Unknown
    return -99;
}

function getEmbedColor(worstHealth) {
    return ['#61b329', '#ff6a00', '#ffc107', '#cc0000'][worstHealth - 1];
}