const { SlashCommandBuilder } = require('@discordjs/builders');
const { MessageEmbed } = require('discord.js');
const { getConfigValue } = require('../configreader.js');
const axios = require('axios').default;

const platformOption = option => 
    option.setName('platform')
        .setDescription('The platform the contract is on')
        .setRequired(true)
        .addChoice('Epic', 'Epic')
        .addChoice('PlayStation', 'PlayStation')
        .addChoice('Stadia', 'Stadia')
        .addChoice('Steam', 'Steam')
        .addChoice('Xbox', 'Xbox');

const contractIdOption = option =>
    option.setName('id')
        .setDescription('The contract ID')
        .setRequired(true);

const data = new SlashCommandBuilder()
    .setName('contract')
    .setDescription('Retrieves contract info from HITMAN™️ 3')
    .addSubcommand(subcommand =>
        subcommand.setName('info')
            .setDescription('Posts a contract to the current channel')
            .addStringOption(platformOption)
            .addStringOption(contractIdOption))
    .addSubcommand(subcommand => 
        subcommand.setName('post')
            .setDescription('Posts a contract to the dedicated contract IDs channel')
            .addStringOption(platformOption)
            .addStringOption(contractIdOption));

module.exports = {
    commandType: 'GLOBAL',
    data: async () => { return data },
    async execute(interaction) {
        const subcommand = interaction.options.getSubcommand();
        await interaction.deferReply();

        if (subcommand === 'post' && !interaction.guild) {
            return await interaction.editReply(':warning: This command cannot be sent via DM. Use `/contract info` instead.');
        }

        const platform = interaction.options.getString('platform');
        const contractId = interaction.options.getString('id');
        
        const contractInfo = await fetchContractFromHitmaps(platform, contractId);

        if (contractInfo === 'ERROR') {
            return await interaction.editReply(getErrorMessage(contractId));
        }
        if (contractInfo === 'AUTHEXPIRED') {
            return await interaction.editReply(getErrorMessage(contractId));
        }
        if (contractInfo === 'NOTFOUND') {
            return await interaction.editReply(`:warning: I couldn't find contract **${contractId}**. Double-check the ID.`);
        }

        const embed = buildContractEmbed(platform, contractInfo);
        
        if (subcommand === 'info') {
            return await interaction.editReply({ embeds: [embed] });
        } 
        
        if (subcommand === 'post') {
            const guildToChannel = {
                '662366684870606848': '',
                '608635330513338419': '701874234024263791'
            };
            let contractChannel = guildToChannel[interaction.guildId];
            if (!contractChannel) {
                return await interaction.editReply(':warning: Configuration Error - Contract posting location not configured.  Contact your server admin.');
            }

            interaction.guild.channels.cache.get(contractChannel).send({ embeds: [embed] });
            return await interaction.editReply(':white_check_mark: Contract posted!');
        }
    }
}

async function fetchContractFromHitmaps(platform, id) {
    try {
        const response = await axios.get(`https://ioiapi.hitmaps.com/api/contracts?platform=${platform}&publicId=${id}`, {
            headers: {
                'X-API-Token': getConfigValue('hitmapsIoiApiToken'),
                'Accept': 'application/json'
            }
        });

        return response.data;
    } catch (error) {
        if (error.response.status === 404) {
            return 'NOTFOUND';
        }
        if (error.response.status === 407) {
            return 'AUTHEXPIRED';
        }

        console.error(error);
        return 'ERROR';
    }
}

function getErrorMessage(publicId) {
    return `:x: An error occurred while trying to retrieve contract **${publicId}**. Use \`/botstatus\` to see if contract retrieval is available for your platform.\n\n` +
        '*Contract availability is subject to community members being willing/able to help keep the bot authenticated.\n' +
        'Want to help keep the bot authenticated? [Install the HITMAPS Bot Authenticator Extension](https://bot.hitmaps.com)';
}

function buildContractEmbed(platform, contractInfo) {
    const platformToEmbedColor = {
        /* #000000 is ignored by Discord */
        Epic: '#000001',
        Steam: '#000001',
        PlayStation: '#003087',
        Xbox: '#0e7a0d',
        Stadia: '#ef3e23',
        Switch: '#ff0000'
    };

    const embed = new MessageEmbed()
        .setColor(platformToEmbedColor[platform])
        .setTitle(contractInfo.name)
        .setFooter({ text: `Contract ID: ${contractInfo.publicId} / Creator: ${contractInfo.creator.displayName} / Platform: ${platform} / Game: HITMAN™️ 3` })
        .addField('Location', contractInfo.location)
        .addField('Briefing', contractInfo.description === '' ? '(No briefing)' : contractInfo.description)
        .addField('Complications', buildComplications(contractInfo))
        .addField('Targets', buildTargetNames(contractInfo));

    if (!contractInfo.objectives.length) {
        embed.setThumbnail('https://media.hitmaps.com/img/hitman3/ui/tiles/defaultmenutilesmall.png');
    } else if (contractInfo.objectives[0].target.image) {
        embed.setThumbnail(contractInfo.objectives[0].target.image);
    } else {
        embed.setThumbnail('https://media.hitmaps.com/img/hitman3/actors/mongoose_unknown_man.png');
    }

    return embed;
}

function buildTargetNames(contractInfo) {
    let targetNames = '```\n';
    for (const objective of contractInfo.objectives) {
        targetNames += `- ${objective.target.name}\n`;
        targetNames += `  - ${objective.killMethod.name}`;

        if (objective.killMethod.subtext) {
            targetNames += ` (${objective.killMethod.subtext})`;
        }
        targetNames += '\n';

        targetNames += `  - ${objective.disguise.name}\n`;
    }
    if (!contractInfo.objectives.length) {
        targetNames += '(No objectives)';
    }
    targetNames += '```';

    return targetNames;
}

function buildComplications(contractInfo) {
    let complications = '';
    for (const complication of contractInfo.complications) {
        complications += '- ';
        if (!complication.required) {
            complications += '[Optional] ';
        }
        complications += `${complication.name}\n`;
    }
    if (!contractInfo.complications.length) {
        complications = '(No complications)';
    }

    return complications;
}