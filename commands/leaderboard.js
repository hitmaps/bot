const { SlashCommandBuilder } = require('@discordjs/builders');
const { fetchLeaderboards } = require('../logic/buildLeaderboard');

const englishIndex = ['', 'first', 'second', 'third', 'fourth', 'fifth', 'sixth']; // Padding since the loop starts at 1

const platformArgs = [];
const contractArgs = [];
for (let i = 1; i <= 6; i++) {
    platformArgs.push(option =>
        option.setName(`platform${i}`)
            .setDescription(`The platform of the ${englishIndex[i]} contract ID`)
            .setRequired(i === 1)
            .addChoice('Epic', 'Epic')
            .addChoice('PlayStation', 'PlayStation')
            .addChoice('Stadia', 'Stadia')
            .addChoice('Steam', 'Steam')
            .addChoice('Switch', 'Switch')
            .addChoice('Xbox', 'Xbox'));
    contractArgs.push(option =>
        option.setName(`contractid${i}`)
            .setDescription(`The ${englishIndex[i]} contract ID`)
            .setRequired(i === 1));
}

const data = new SlashCommandBuilder()
    .setName('leaderboard')
    .setDescription('Retrieves leaderboard info from HITMAN™️ 3');

for (let i = 0; i < 6; i++) {
    data.addStringOption(platformArgs[i]);
    data.addStringOption(contractArgs[i]);
}

module.exports = {
    commandType: 'GLOBAL',
    data: async () => { return data },
    async execute(interaction) {
        await interaction.deferReply();

        const platforms = [];
        const contractIds = [];

        for (let i = 1; i <= 6; i++) {
            const platform = interaction.options.getString(`platform${i}`);
            const contractId = interaction.options.getString(`contractid${i}`);
            if (platform && contractId) {
                platforms.push(platform);
                contractIds.push(contractId);
            }
        }

        if (!platforms.length && !contractIds.length) {
            return await interaction.editReply('You must enter at least 1 platform / contract ID pair!');
        }

        const leaderboardEmbed = await fetchLeaderboards(platforms, contractIds);

        if (leaderboardEmbed === 'ERROR') {
            return await interaction.editReply(':x: An error occurred while fetching leaderboards.');
        }
        
        return await interaction.editReply(leaderboardEmbed);
    }
}