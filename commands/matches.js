const { SlashCommandBuilder } = require('@discordjs/builders');
const { default: axios } = require('axios');
const { MessageEmbed } = require('discord.js');

module.exports = {
    commandType: 'GLOBAL',
    data: async () => {
        const activeEvents = await getActiveEvents();

        return new SlashCommandBuilder()
            .setName('matches')
            .setDescription('Retrieves upcoming tournament matches from HITMAPS™ Tournaments')
            .addStringOption(option => {
                option.setName('event')
                    .setDescription('The event to fetch upcoming matches for')
                    .setRequired(true);

                activeEvents.forEach(activeEvent => option.addChoice(activeEvent.name, activeEvent.slug));
                
                return option;
            }).addIntegerOption(option =>
                option.setName('limit')
                    .setDescription('Limit the number of matches to return (min = 1, max = 8)')
                    .setRequired(false)
                    .setMinValue(1)
                    .setMaxValue(8));
    },
    async execute(interaction) {
        await interaction.deferReply();

        const limit = interaction.options.getInteger('limit') ?? 8;
        const eventSlug = interaction.options.getString('event');
        if (eventSlug === '-1' || eventSlug === '-2') {
            return await interaction.editReply(':warning: I\'m not able to fetch information about that event.');
        }

        const eventUrl = `https://tournamentsapi.hitmaps.com/api/events/${eventSlug}`;
        const url = `https://tournamentsapi.hitmaps.com/api/events/${eventSlug}/upcoming-matches`;
        let response = null;
        let eventResponse = null;

        try {
            response = await axios.get(url);
            eventResponse = await axios.get(eventUrl);
        } catch (e) {
            return await interaction.editReply(':x: An error occurred when fetching upcoming matches');
        }

        const eventName = eventResponse.data.event.name;
        let footerText = 'View the complete list of upcoming matches over at HITMAPS.com';
        if (interaction.options.getInteger('limit') !== null) {
            const matches = limit === 1 ? 'match' : 'matches';
            footerText = `Only the next ${limit} ${matches} shown.\n${footerText}`;
        }
        const embed = new MessageEmbed()
            .setColor('#000001')
            .setTitle(`${eventName} - Upcoming Matches`)
            .setFooter({ text: footerText });

        let currentIndex = 0;
        for (const entry of response.data.data) {
            if (++currentIndex > limit) {
                break;
            }

            const mapSelections = entry.maps
                .filter(x => x.selectionType === 'Pick')
                .map(x => `${x.missionLocation} - ${x.missionName}`);
            const matchScheduledAt = parseInt(new Date(entry.matchScheduledAt).getTime() / 1000, 10);
            const casters = [];
            if (entry.cast !== null) {
                casters.push(entry.cast.mainCaster.name);
                for (const cocaster of entry.cast.cocasters) {
                    casters.push(cocaster.name);
                }
            }

            const formattedMaps = mapSelections.join('\n');
            embed.addField('\u200b', `**${entry.competitors[0].challongeName}** vs **${entry.competitors[1].challongeName}**\n*${formattedMaps}*`, true);

            if (entry.matchAdmin !== null) {
                embed.addField('\u200b', `:clock12: <t:${matchScheduledAt}:f>\n:judge: ${discordEscapeString(entry.matchAdmin.name)}`, true);
            } else {
                embed.addField('\u200b', `:clock12: <t:${matchScheduledAt}:f>`, true);
            }

            const formattedCasters = casters.length ? 
            `:mega: [${casters.join('\n')}](${entry.cast.mainCasterUrl})` : 
            '\u200b';
            embed.addField('\u200b', formattedCasters, true);
        }

        return await interaction.editReply({ embeds: [embed] });
    }
};

function discordEscapeString(str) {
    if (!str) {
        return str;
    }

    //-- Replace _ with \_
    return str.replaceAll('_', '\\_').replaceAll('*', '\\*').replaceAll('#', '\\#').replaceAll('~', '\\~');
}

async function getActiveEvents() {
    const url = 'https://tournamentsapi.hitmaps.com/api/events';
    let response = null;

    const events = [];
    try {
        response = await axios.get(url);

        const activeEvents = response.data.filter(x => new Date(x.isoRegistrationEndDate) < new Date() && 
            new Date(x.isoEventEndDate) > new Date());
        for (const activeEvent of activeEvents) {
            events.push(activeEvent);
        }
    } catch (e) {
        events.push({ name: '[ERROR] Could not fetch events. Contact HITMAPS™', slug: '-1' });
    }

    if (!events.length) {
        events.push({ name: 'There are no active tournaments.', slug: '-2'});
    }

    return events;
}