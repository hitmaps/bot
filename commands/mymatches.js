const { SlashCommandBuilder } = require('@discordjs/builders');
const { default: axios } = require('axios');
const { MessageEmbed } = require('discord.js');

module.exports = {
    commandType: 'GLOBAL',
    data: async () => {
        const activeEvents = await getActiveEvents();

        return new SlashCommandBuilder()
            .setName('mymatches')
            .setDescription('Retrieves information about your completed / scheduled matches for a given tournament')
            .addStringOption(option => {
                option.setName('event')
                    .setDescription('The event to fetch upcoming matches for')
                    .setRequired(true);

                activeEvents.forEach(activeEvent => option.addChoice(activeEvent.name, activeEvent.slug));
                
                return option;
            });
    },
    async execute(interaction) {
        await interaction.deferReply();

        const eventSlug = interaction.options.getString('event');
        if (eventSlug === '-1' || eventSlug === '-2') {
            return await interaction.editReply(':warning: I\'m not able to fetch information about that event.');
        }

        const url = `https://tournamentsapi.hitmaps.com/api/events/${eventSlug}/participants/${interaction.user.id}/match-history`;
        let response = null;
        try {
            response = await axios.get(url);

            var viewModels = response.data;
            var embeds = [];
            for (const viewModel of viewModels) {
                const tooManyMatches = viewModel.matches.length > 8;
                const matchesToShow = tooManyMatches ? 
                    viewModel.matches.slice(viewModel.matches.length - 8, viewModel.matches.length) : 
                    viewModel.matches;
                let footerText = 'Scheduled matches will show all maps (picks/bans/random).\nCompleted matches will only show your picks/bans.';
                if (tooManyMatches) {
                    footerText += '\nNote: Only the most recent 8 matches are shown due to Discord limitations.';
                }

                const embed = new MessageEmbed()
                    .setColor('#000001')
                    .setTitle(viewModel.tournamentName)
                    .setFooter({ text: footerText });

                
                for (const match of matchesToShow) {
                    // Basic Match Info
                    let round = null;
                    if (match.groupStage) {
                        round = 'Group Stage';
                    } else {
                        round = match.round > 0 ? `Round ${match.round}` : `LB Round ${Math.abs(match.round)}`;
                    }
                    const opponent = match.opponentDiscordId === null ? '' : ` vs <@${match.opponentDiscordId}>`;

                    let leftHandSide = `**${round}${opponent}**`;

                    // Status
                    let status = '';
                    let maps = '';
                    if (match.status === 'complete') {
                        status = ':checkered_flag: Complete';
                        maps = match.maps
                            .filter(x => x.chosenByDiscordId === interaction.user.id)
                            .map(x => formatMapPick(x, false))
                            .join('\n');
                    } else if (match.status === 'scheduled') {
                        status = ':white_check_mark: Scheduled';
                        maps = match.maps
                            .filter(x => x.selectionType !== 'Ban' && x.missionName !== null)
                            .map(x => formatMapPick(x, true))
                            .join('\n');
                        const randomHiddenMaps = match.maps.filter(x => x.selectionType !== 'Ban' && x.missionName === null);
                        if (randomHiddenMaps.length) {
                            maps += `\n+${randomHiddenMaps.length} hidden random maps`;
                        }
                        leftHandSide += `\n<t:${parseInt(new Date(match.matchScheduledAt).getTime() / 1000, 10)}:f>`;
                    } else if (match.status === 'not-scheduled') {
                        status = ':warning: Needs Scheduling';
                    } else if (match.status === 'not-schedulable') {
                        status = ':no_entry: Waiting for Opponent';
                    } else {
                        status = ':question: Unknown';
                    }

                    let rightHandSide = `${status}`;
                    if (maps !== '') {
                        rightHandSide += `\n*${maps}*`;
                    }
                    embed.addField('\u200b', leftHandSide, true);
                    embed.addField('\u200b', rightHandSide, true);
                    embed.addField('\u200b', '\u200b', true);
                }
                embeds.push(embed);
            }
        } catch (e) {
            console.error(e);
            return await interaction.editReply(':x: An error occurred when fetching your match information');
        }

        return await interaction.editReply({ embeds: embeds });
    }
};

function formatMapPick(mapPick, showChosenBy) {
    let output = '';
    if (!showChosenBy) {
        output += `**${mapPick.selectionType}**: `;
    }

    output += `${mapPick.missionLocation} - ${mapPick.missionName}`;

    if (showChosenBy && mapPick.chosenByDiscordId !== null) {
        output += ` (<@${mapPick.chosenByDiscordId}>'s Choice)`;
    }

    return output;
}

async function getActiveEvents() {
    const url = 'https://tournamentsapi.hitmaps.com/api/events';
    let response = null;

    const events = [];
    try {
        response = await axios.get(url);

        const activeEvents = response.data.filter(x => new Date(x.isoRegistrationEndDate) < new Date() && 
            new Date(x.isoEventEndDate) > new Date());
        for (const activeEvent of activeEvents) {
            events.push(activeEvent);
        }
    } catch (e) {
        events.push({ name: '[ERROR] Could not fetch events. Contact HITMAPS™', slug: '-1' });
    }

    if (!events.length) {
        events.push({ name: 'There are no active tournaments.', slug: '-2'});
    }

    return events;
}