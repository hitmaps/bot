const { SlashCommandBuilder } = require('@discordjs/builders');

module.exports = {
    commandType: 'GLOBAL',
    data: async () => { 
        return new SlashCommandBuilder()
            .setName('ping')
            .setDescription('Replies with Pong!')
    },
    async execute(interaction) {
        await interaction.reply('Pong!');
    }
};