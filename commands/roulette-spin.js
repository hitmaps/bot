const { SlashCommandBuilder } = require('@discordjs/builders');
const { default: axios } = require('axios');
const { MessageEmbed } = require('discord.js');

module.exports = {
    commandType: 'GLOBAL',
    data: async () => { 
        return new SlashCommandBuilder()
            .setName('roulette-spin')
            .setDescription('Generates a roulette spin')
            .addSubcommand(subcommand =>
                subcommand.setName('hitman')
                    .setDescription('Generates a roulette spin from a HITMAN™️ mission')
                    .addStringOption(option =>
                        option.setName('mission')
                            .setDescription('The mission to generate a spin for')
                            .setRequired(true)
                            .addChoice('ICA Facility - Freeform Training', 'freeform-training')
                            .addChoice('ICA Facility - The Final Test', 'the-final-test')
                            .addChoice('Paris - The Showstopper', 'the-showstopper')
                            .addChoice('Sapienza - World of Tomorrow', 'world-of-tomorrow')
                            .addChoice('Sapienza - The Icon', 'the-icon')
                            .addChoice('Sapienza - Landslide', 'landslide')
                            .addChoice('Sapienza - The Author', 'the-author')
                            .addChoice('Marrakesh - A Gilded Cage', 'a-gilded-cage')
                            .addChoice('Marrakesh - A House Built on Sand', 'a-house-built-on-sand')
                            .addChoice('Bangkok - Club 27', 'club-27')
                            .addChoice('Bangkok - The Source', 'the-source')
                            .addChoice('Colorado - Freedom Fighters', 'freedom-fighters')
                            .addChoice('Hokkaido - Situs Inversus', 'situs-inversus')
                            .addChoice('Hokkaido - Hokkaido Snow Festival', 'hokkaido-snow-festival')
                            .addChoice('Hokkaido - Patient Zero', 'patient-zero')))
            .addSubcommand(subcommand =>
                subcommand.setName('hitman2')
                    .setDescription('Generates a roulette spin from a HITMAN™️ 2 mission')
                    .addStringOption(option =>
                        option.setName('mission')
                            .setDescription('The mission to generate a spin for')
                            .setRequired(true)
                            .addChoice("Hawke's Bay - Nightcall", 'nightcall')
                            .addChoice('Miami - The Finish Line', 'finish-line')
                            .addChoice('Miami - A Silver Tongue', 'a-silver-tongue')
                            .addChoice('Santa Fortuna - Three-Headed Serpent', 'three-headed-serpent')
                            .addChoice('Santa Fortuna - Embrace of the Serpent', 'embrace-of-the-serpent')
                            .addChoice('Mumbai - Chasing A Ghost', 'chasing-a-ghost')
                            .addChoice('Mumbai - Illusions of Grandeur', 'illusions-of-grandeur')
                            .addChoice('Whittleton Creek - Another Life', 'another-life')
                            .addChoice('Whittleton Creek - A Bitter Pill', 'a-bitter-pill')
                            .addChoice('Isle of Sgàil - The Ark Society', 'ark-society')
                            .addChoice('New York - Golden Handshake', 'golden-handshake')
                            .addChoice('Haven Island - The Last Resort', 'the-last-resort')))
            .addSubcommand(subcommand =>
                subcommand.setName('hitman3')
                    .setDescription('Generates a roulette spin from a HITMAN™️ 3 mission')
                    .addStringOption(option =>
                        option.setName('mission')
                            .setDescription('The mission to generate a spin for')
                            .setRequired(true)
                            .addChoice('Dubai - On Top Of The World', 'on-top-of-the-world')
                            .addChoice('Dartmoor - Death In The Family', 'death-in-the-family')
                            .addChoice('Berlin - Apex Predator', 'apex-predator')
                            .addChoice('Chongqing - End Of An Era', 'end-of-an-era')
                            .addChoice('Mendoza - The Farewell', 'the-farewell')
                            .addChoice('Carpathian Mountains - Untouchable', 'untouchable')
                            .addChoice('Ambrose Island - Shadows in the Water', 'shadows-in-the-water')))
    },
    async execute(interaction) {
        await interaction.deferReply();
        const slug = interaction.options.getString('mission');
        const url = 'https://rouletteapi.hitmaps.com/api/spins';
        const postData = {
            criteriaFilters: {
                specificDisguises: true,
                specificMelee: true,
                specificFirearms: true,
                specificAccidents: true,
                uniqueTargetKills: false,
                genericKills: false,
                impossibleOrDifficultKills: false,
                additionalObjectives: false,
                additionalObjectiveDisguises: false
            },
            missionPool: getMissionPoolCriteriaForSlug(slug)
        };
        

        let response;
        try {
            response = await axios.post(url, postData);
        } catch (e) {
            return await interaction.editReply(':x: An error occurred when fetching statuses');
        }

        const data = response.data;

        const embed = new MessageEmbed()
            .setColor('#000001')
            .setTitle(data.mission.name);

        for (const targetCondition of data.targetConditions) {
            let formattedKillMethod = targetCondition.killMethod.name;
            if (targetCondition.killMethod.selectedVariant) {
                formattedKillMethod = `**${targetCondition.killMethod.selectedVariant}** ${formattedKillMethod}`;
            }
            const formattedCondition = `:crossed_swords: ${formattedKillMethod}\n:disguised_face: ${targetCondition.disguise.name}`;
            embed.addField(targetCondition.target.name, formattedCondition);
        }

        return await interaction.editReply({ embeds: [embed] });
    }
};

function getMissionPoolCriteriaForSlug(slug) {
    const mapping = {
        'freeform-training': 'hitman|ica-facility|freeform-training',
        'the-final-test': 'hitman|ica-facility|the-final-test',
        'the-showstopper': 'hitman|paris|the-showstopper',
        'world-of-tomorrow': 'hitman|sapienza|world-of-tomorrow',
        'the-icon': 'hitman|sapienza|the-icon',
        'landslide': 'hitman|sapienza|landslide',
        'the-author': 'hitman|sapienza|the-author',
        'a-gilded-cage': 'hitman|marrakesh|a-gilded-cage',
        'a-house-built-on-sand': 'hitman|marrakesh|a-house-built-on-sand',
        'club-27': 'hitman|bangkok|club-27',
        'the-source': 'hitman|bangkok|the-source',
        'freedom-fighters': 'hitman|colorado|freedom-fighters',
        'situs-inversus': 'hitman|hokkaido|situs-inversus',
        'hokkaido-snow-festival': 'hitman|hokkaido|hokkaido-snow-festival',
        'patient-zero': 'hitman|hokkaido|patient-zero',
        'nightcall': 'hitman2|hawkes-bay|nightcall',
        'finish-line': 'hitman2|miami|finish-line',
        'a-silver-tongue': 'hitman2|miami|a-silver-tongue',
        'three-headed-serpent': 'hitman2|santa-fortuna|three-headed-serpent',
        'embrace-of-the-serpent': 'hitman2|santa-fortuna|embrace-of-the-serpent',
        'chasing-a-ghost': 'hitman2|mumbai|chasing-a-ghost',
        'illusions-of-grandeur': 'hitman2|mumbai|illusions-of-grandeur',
        'another-life': 'hitman2|whittleton-creek|another-life',
        'a-bitter-pill': 'hitman2|whittleton-creek|a-bitter-pill',
        'ark-society': 'hitman2|isle-of-sgail|ark-society',
        'golden-handshake': 'hitman2|new-york|golden-handshake',
        'the-last-resort': 'hitman2|haven-island|the-last-resort',
        'on-top-of-the-world': 'hitman3|dubai|on-top-of-the-world',
        'death-in-the-family': 'hitman3|dartmoor|death-in-the-family',
        'apex-predator': 'hitman3|berlin|apex-predator',
        'end-of-an-era': 'hitman3|chongqing|end-of-an-era',
        'the-farewell': 'hitman3|mendoza|the-farewell',
        'untouchable': 'hitman3|carpathian-mountains|untouchable',
        'shadows-in-the-water': 'hitman3|ambrose-island|shadows-in-the-water'
    };

    return slug === 'surprise' ? Object.values(mapping) : [mapping[slug]];
}