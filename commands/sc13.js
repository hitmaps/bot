const { SlashCommandBuilder } = require('@discordjs/builders');
const { fetchLeaderboards } = require('../logic/buildLeaderboard');

const data = new SlashCommandBuilder()
    .setName('sc13')
    .setDescription('Retrieves leaderboard info for Speedrun Comp 13')
    .addSubcommand(subcommand =>
        subcommand.setName('marrakesh')
            .setDescription('Fetches leaderboard info for SC13 - Marrakesh'))
    .addSubcommand(subcommand =>
        subcommand.setName('isle-of-sgail')
            .setDescription('Fetches leaderboard info for SC13 - Isle of Sgàil󠁧󠁢󠁳'))
    .addSubcommand(subcommand =>
        subcommand.setName('chongqing')
            .setDescription('Fetches leaderboard info for SC13 - Chongqing'));

module.exports = {
    commandType: 'GUILD',
    data: async () => { return data },
    async execute(interaction) {
        const subcommand = interaction.options.getSubcommand();

        await interaction.deferReply();

        const platforms = ['Epic', 'Steam', 'PlayStation', 'Xbox', 'Switch'];
        let contractIds = [];
        switch (subcommand) {
            case 'marrakesh':
                contractIds = ['1-06-1034316-04','1-06-8904109-52','2-06-0682642-47','3-06-0966671-72','4-06-5143624-20'];
                break;
            case 'isle-of-sgail':
                contractIds = ['1-21-5752377-04','1-21-2517188-52','2-21-8407361-47','3-21-9733428-72','4-21-1629427-20'];
                break;
            case 'chongqing':
                contractIds = ['1-30-1710606-04','1-30-9484752-52','2-30-6804877-47','3-30-8497884-72','4-30-0997806-20'];
                break;
        }

        const leaderboardEmbed = await fetchLeaderboards(platforms, contractIds);

        if (leaderboardEmbed === 'ERROR') {
            return await interaction.editReply(':x: An error occurred while fetching leaderboards.');
        }
        
        return await interaction.editReply(leaderboardEmbed);
    }
}
