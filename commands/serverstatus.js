const { SlashCommandBuilder } = require('@discordjs/builders');
const { default: axios } = require('axios');
const { MessageEmbed } = require('discord.js');

module.exports = {
    commandType: 'GLOBAL',
    data: async () => { 
        return new SlashCommandBuilder()
            .setName('serverstatus')
            .setDescription('Retrieves the current status of HITMAN™️ services')
            .addStringOption(option => 
                option.setName('game')
                    .setDescription('The game to retrieve server status for')
                    .setRequired(true)
                    .addChoice('HITMAN™ World of Assassination', 'woa')
                    .addChoice('HITMAN™ 2 (Legacy)', 'hitman2')
                    .addChoice('HITMAN™ (Legacy)', 'hitman'))
    },
    async execute(interaction) {
        await interaction.deferReply();
        const game = interaction.options.getString('game');
        const url = 'https://auth.hitman.io/status';
        const reportsUrl = 'https://hitmanstat.us/reports';

        let response;
        let reportsResponse;
        try {
            response = await axios.get(url);
            reportsResponse = await axios.get(reportsUrl);
        } catch (e) {
            return await interaction.editReply(':x: An error occurred when fetching statuses');
        }

        const data = response.data.services;
        const reportsData = reportsResponse.data.series;

        let statuses = [];
        let title = '';
        if (game === 'woa') {
            title = 'HITMAN:tm: World of Assassination Server Status';
            statuses.push({
                platform: 'Steam',
                health: getServerHealth(data['steam.hm3-service.hitman.io'], reportsData[0].data[6])
            });
            statuses.push({
                platform: 'Epic',
                health: getServerHealth(data['epic.hm3-service.hitman.io'], reportsData[1].data[6])
            });
            statuses.push({
                platform: 'PlayStation',
                health: getServerHealth(data['ps.hm3-service.hitman.io'], reportsData[3].data[6])
            });
            statuses.push({
                platform: 'Xbox',
                health: getServerHealth(data['xbox.hm3-service.hitman.io'], reportsData[2].data[6])
            });
            statuses.push({
                platform: 'Switch',
                health: getServerHealth(data['switch.hm3-service.hitman.io'], reportsData[4].data[6])
            });
        } else if (game === 'hitman2') {
            title = 'HITMAN:tm: 2 Server Status';
            statuses.push({
                platform: 'Steam',
                health: getServerHealth(data['pc2-service.hitman.io'], 0)
            });
            statuses.push({
                platform: 'PlayStation',
                health: getServerHealth(data['ps42-service.hitman.io'], 0)
            });
            statuses.push({
                platform: 'Xbox',
                health: getServerHealth(data['xboxone2-service.hitman.io'], 0)
            });
        } else if (game === 'hitman') {
            title = 'HITMAN:tm: Server Status';
            statuses.push({
                platform: 'Steam',
                health: getServerHealth(data['pc-service.hitman.io'], 0)
            });
            statuses.push({
                platform: 'PlayStation',
                health: getServerHealth(data['ps4-service.hitman.io'], 0)
            });
            statuses.push({
                platform: 'Xbox',
                health: getServerHealth(data['xboxone-service.hitman.io'], 0)
            });
        }

        const embed = new MessageEmbed()
            .setColor(getEmbedColor(game, data))
            .setTitle(title)
            .setURL('https://hitmanstat.us')
            .setFooter({ text: "User reports are from the current day and are tracked by hitmanstat.us\n\nKeep in mind that Hitman's services also rely on Azure's infrastructure and third-party digital distribution platforms services. If Hitman's servers appear online here but you still have some issues, you can check Steam, Epic Games, PSN, Xbox Live and Azure status pages to get more information." })
            
        for (const status of statuses) {
            embed.addField(status.platform, status.health, true);
        }

        return await interaction.editReply({ embeds: [embed] });
    }
};

function getClockEmoji(time) {
    let hourOfDay = parseInt(time.substring(11, 13), 10);
    const minuteOfHour = parseInt(time.substring(14, 16), 10);
    let displayMinuteOfHour = '';
    if (hourOfDay > 12) {
        hourOfDay -= 12;
    } else if (hourOfDay === 0) {
        hourOfDay = 12;
    }

    if ((minuteOfHour > 0 && minuteOfHour < 16) || (minuteOfHour > 45)) {
        displayMinuteOfHour = '';
    } else if (minuteOfHour > 15 && minuteOfHour < 44) {
        displayMinuteOfHour = '30';
    }

    return `:clock${hourOfDay}${displayMinuteOfHour}:`;
}

function getServerHealth(platform, reportCount) {
    let status = ':man_shrugging: Unknown'
    const reportsSuffix = reportCount !== 1 ? 'users reported issues' : 'user reported issues';
    const reports = reportCount === 0 ? '' : `\n:bar_chart: ${reportCount} ${reportsSuffix}`;

    if (platform.health === 'healthy') {
        status = ':white_check_mark: Online';
    } else if (platform.health === 'maintenance') {
        const startTime = parseInt(new Date(platform.nextWindow.start).getTime() / 1000, 10);
        const endTime = parseInt(new Date(platform.nextWindow.end).getTime() / 1000, 10);
        status = `:construction: Maintenance\n**Started**: <t:${startTime}:t>\n**Ends**: <t:${endTime}:t>`;
    } else if (platform.health === 'down') {
        status = ':x: Down';
    } else if (platform.health === 'slow') {
        status = ':warning: Online - High Load';
    }

    if (platform.nextWindow !== null && platform.health !== 'maintenance') {
        const startTime = parseInt(new Date(platform.nextWindow.start).getTime() / 1000, 10);
        status += `\n**Maintenance**: <t:${startTime}:R>`;
    }

    return `${status}${reports}`;
}

function getEmbedColor(game, services) {
    const statusToNumber = {
        healthy: 1,
        slow: 2,
        maintenance: 3,
        down: 4
    };
    const statusNumberToColor = ['SKIP', '#61b329', '#ff6a00', '#ffc107', '#cc0000'];

    if (game === 'woa') {
        const epicHealth = statusToNumber[services['epic.hm3-service.hitman.io']['health']];
        const steamHealth = statusToNumber[services['steam.hm3-service.hitman.io']['health']];
        const psHealth = statusToNumber[services['ps.hm3-service.hitman.io']['health']];
        const xboxHealth = statusToNumber[services['xbox.hm3-service.hitman.io']['health']];
        const switchHealth = statusToNumber[services['switch.hm3-service.hitman.io']['health']];
        return statusNumberToColor[Math.max(epicHealth, steamHealth, psHealth, xboxHealth, switchHealth)];
    }
    
    if (game === 'hitman2') {
        const steamHealth = statusToNumber[services['pc2-service.hitman.io']['health']];
        const psHealth = statusToNumber[services['ps42-service.hitman.io']['health']];
        const xboxHealth = statusToNumber[services['xboxone2-service.hitman.io']['health']];
        return statusNumberToColor[Math.max(steamHealth, psHealth, xboxHealth)];
    }
    
    const steamHealth = statusToNumber[services['pc-service.hitman.io']['health']];
    const psHealth = statusToNumber[services['ps4-service.hitman.io']['health']];
    const xboxHealth = statusToNumber[services['xboxone-service.hitman.io']['health']];
    return statusNumberToColor[Math.max(steamHealth, psHealth, xboxHealth)];
}
