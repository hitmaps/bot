module.exports = {
    getConfigValue(key) {
        return process.env[key] ?? module.exports.getFromConfigJson(key);
    },
    getFromConfigJson(key) {
        const config = require('./config.json');

        return config[key];
    }
}