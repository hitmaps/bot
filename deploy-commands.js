const fs = require('node:fs');
const { REST } = require('@discordjs/rest');
const { Routes } = require('discord-api-types/v9');
const { getConfigValue } = require('./configreader.js');

const globalCommands = [];
const guildCommands = [];
const commandFiles = fs.readdirSync('./commands').filter(file => file.endsWith('.js'));

for (const file of commandFiles) {
    const command = require(`./commands/${file}`);

    if (command.commandType === 'GLOBAL') {
        globalCommands.push(command.data());
    } else if (command.commandType === 'GUILD') {
        guildCommands.push(command.data());
    }
}
    

const rest = new REST({ version: '9' }).setToken(getConfigValue('token'));

if (getConfigValue('environment') === 'PRODUCTION') {
    // 1. Apply global commands
    Promise.all(globalCommands).then(values => {
        const requestBody = values.map(x => x.toJSON());

        rest.put(Routes.applicationCommands(getConfigValue('clientId')), { body: requestBody })
            .then(() => console.log('[PROD] Successfully registered global commands.'))
            .catch(console.error);
    });

    // 2. Also apply guild-specific commands
    Promise.all(guildCommands).then(values => {
        const requestBody = values.map(x => x.toJSON());

        rest.put(Routes.applicationGuildCommands(getConfigValue('clientId'), getConfigValue('guildId')), { body: requestBody })
            .then(() => console.log('[PROD] Successfully registered application commands.'))
            .catch(console.error);
    });
    
} else {
    const allCommands = [...globalCommands, ...guildCommands];
    Promise.all(allCommands).then(values => {
        const requestBody = values.map(x => {
            return x.toJSON();
        });

        rest.put(Routes.applicationGuildCommands(getConfigValue('clientId'), getConfigValue('guildId')), { body: requestBody })
            .then(() => console.log('[DEV]  Successfully registered all commands to the guild.'))
            .catch(console.error);
    });
}
