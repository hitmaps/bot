const { exec } = require('child_process');

module.exports = {
    startCommandDeployer() {
        const SIX_HOURS = 6 * 60 * 60 * 1000;

        module.exports.doExec();

        setInterval(
            () => module.exports.doExec(),
            SIX_HOURS);
    },
    doExec() {
        exec('node ./deploy-commands.js', (error, stdout, stderror) => {
            console.log(stdout, stderror, error);
        });
    }
}