const axios = require('axios').default;
const { getConfigValue } = require('../configreader.js');
const fs = require('fs');
const path = require('path');
const decodeEntities = require('decode-entities');
const Keyv = require('keyv');

module.exports = {
    async startIoiFeedWatcherAsync() {
        const keyv = new Keyv(getConfigValue('redisConnectionString'));

        try {
            await postNewIoiArticle(keyv);
        } catch (e) {
            console.error(e);
        }

        setInterval(async () => {
            try {
                await postNewIoiArticle(keyv);
            } catch (e) {
                console.error(e);
            }
        }, 60000);
    }
}

async function postNewIoiArticle(keyv) {
    const latestArticle = await grabLatestArticleFromIoiAsync();

    let storedValues = {};

    try {
        storedValues = JSON.parse(await keyv.get('bot:stored-values'));
    } catch (e) {
        //-- Ignored
    }
    
    const storedValuesKey = `${latestArticle.title}|${latestArticle.publishDate}`;

    if (storedValuesKey === storedValues.latestIoiArticleKey) {
        return;
    }

    //-- New article. Post and update storedvalues.
    const articleUri = latestArticle.link.url.replace('https://ioi.dk', '');
    let articleContentResponse;
    try {
        articleContentResponse = await axios.get(`https://ioi.dk/umbraco/pages?url=${articleUri}`);
    } catch (error) {
        console.error(error);
        return;
    }
    const textBlock = articleContentResponse.data.properties.blocks.find(x => x.type === 'TextBlock');
    let previewText = ' ';
    if (textBlock && textBlock.value) {
        // Thanks AnthonyFuller / Notex :)
        previewText = textBlock.value.blocks[0].value.text;
        if (!previewText) {
            previewText = textBlock.value.blocks[0].value.statementText;
        }
        if (!previewText) {
            //-- Ignore it
            return;
        }
        previewText = decodeEntities(previewText)
            .replace(/(<([^>]+)>)/gi, "")
            .replace(/(\r\n|\n|\r)/gm, " ")
            .trim();
        previewText = previewText.length > 250 ? previewText.substring(0, 250) + '…' : previewText;
    }
    

    const embed = {
        title: latestArticle.title,
        url: latestArticle.link.url,
        description: previewText,
        footer: {
            text: latestArticle.link.url
        },
        timestamp: latestArticle.publishDate
    }

    if (latestArticle.media.length && latestArticle.media[0].value.image) {
        embed.image = { 
            url: latestArticle.media[0].value.image.umbracoFile.src 
        };
    }

    try {
        await axios.post(getConfigValue('ioiFeedWebhookUrl'), {
            embeds: [embed]
        });
        storedValues.latestIoiArticleKey = storedValuesKey;
        await keyv.set('bot:stored-values', JSON.stringify(storedValues));
    } catch (error) {
        console.error(error);
    }
}

async function grabLatestArticleFromIoiAsync() {
    try {
        const url = 'https://ioi.dk/umbraco/pages/article-explore-panels?filter.type.group=type&filter.type.tags=all&filter.universe.group=*&filter.universe.tags[]=&filter.location.group=location&filter.location.tags[]=%23%23all_location%23%23&pagination.limit=1';
        const response = await axios.get(url);

        return response.data.cards[0];
    } catch (error) {
        console.error(error);
        return;
    }
}