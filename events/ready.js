const { startIoiFeedWatcherAsync } = require('./_ioiFeedWatcher');
const { startCommandDeployer } = require('./_commandDeployer');

module.exports = {
    name: 'ready',
    once: true,
    execute(client) {
        console.log(`Ready! Logged in as ${client.user.tag}`);

        startIoiFeedWatcherAsync();
        startCommandDeployer();
    }
};