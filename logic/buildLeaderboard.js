const { MessageEmbed } = require('discord.js');
const { getConfigValue } = require('../configreader.js');
const axios = require('axios').default;

async function fetchLeaderboards(platforms, contractIds) {
    const leaderboardInfo = await fetchLeaderboardFromHitmaps(platforms, contractIds);

    if (leaderboardInfo === 'ERROR') {
        return 'ERROR';
    }

    let embed = null;
    let message = null;

    if (leaderboardInfo.contractIds.length) {
        embed = buildLeaderboardEmbed(platforms[0], leaderboardInfo);
    }
    if (leaderboardInfo.failedContractIds.length) {
        message = `:warning: I failed to fetch leaderboards for the following contracts: **${leaderboardInfo.failedContractIds.join(', ')}**.\n` + 
                  ':information_source: **Want to help keep the bot authenticated?** [Install the HITMAPS Bot Authenticator Extension](https://bot.hitmaps.com)';
    }
    
    const response = {};
    if (embed !== null) {
        response.embeds = [embed];
    }
    if (message !== null) {
        response.content = message;
    }
    
    return response;
}

async function fetchLeaderboardFromHitmaps(platforms, ids) {
    try {
        const response = await axios.get(`https://ioiapi.hitmaps.com/api/leaderboards?platforms=${platforms.join()}&publicIds=${ids.join()}`, {
            headers: {
                'X-API-Token': getConfigValue('hitmapsIoiApiToken'),
                'Accept': 'application/json'
            }
        });

        return response.data;
    } catch (error) {
        console.error(error);
        return 'ERROR';
    }
}

function buildLeaderboardEmbed(platform, leaderboardInfo) {
    const platformToEmbedColor = {
        /* #000000 is ignored by Discord */
        Epic: '#000001',
        Steam: '#000001',
        PlayStation: '#003087',
        Xbox: '#0e7a0d',
        Switch: '#ff0000'
    };
    const platformToEmoji = {
        Epic: '<:epic:1136368951887134841>',
        Steam: '<:steam:1136368954252722216>',
        PlayStation: '<:playstation:1136368953162203176>',
        Xbox: '<:xbox:1136368950175875122>',
        Switch: '<:switch:1136368955259367458>'
    }

    let contractIdHeader = 'Contract IDs';
    let locationHeader = 'Locations';
    if (leaderboardInfo.contractIds.length === 1) {
        contractIdHeader = 'Contract ID';
        nameHeader = 'Name';
        locationHeader = 'Location';
    }
    
    const embed = new MessageEmbed()
        .setColor(platformToEmbedColor[platform])
        .setTitle(leaderboardInfo.uniqueContractNames.join(', '))
        .setFooter({ text: `${contractIdHeader}: ${leaderboardInfo.contractIds.join(', ')} / ${locationHeader}: ${leaderboardInfo.uniqueContractLocations.join(', ')}` })
        .setTimestamp(new Date(leaderboardInfo.accessTime));

    const namePrefix = [':first_place:', ':second_place:', ':third_place:', '4 |', '5 |', '6 |', '7 |', '8 |', '9 |', '10 |', '11 |', '12 |', '13 |', '14 |', '15 |'];
    for (let i = 0; i < Math.min(15, leaderboardInfo.entries.length); i++) {
        const entry = leaderboardInfo.entries[i];

        //const value = `${entry.platform}\n${entry.elapsedTime}\n${entry.totalScore}`;
        const value = `${entry.elapsedTime} (${entry.totalScore})`;
        embed.addField(`${namePrefix[i]} ${entry.playerName} ${platformToEmoji[entry.platform]}`, value, true);
    }

    return embed;
}

module.exports = {
    fetchLeaderboards
};