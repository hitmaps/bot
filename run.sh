#!/bin/bash
# This file should be ran as a service
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
cd $DIR
exec node index.js
